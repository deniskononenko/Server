package main

import (
	"github.com/gorilla/mux"
	"net/http"
	"fmt"
	"html/template"
)

type ContactDetails struct {
	Email   string
	Subject string
	Message string
}

func main() {

	r := mux.NewRouter()

	r.HandleFunc("/", func (w http.ResponseWriter, r *http.Request) {
		tmpl, err := template.ParseFiles("view/main.html")
		if err != nil {
			fmt.Fprintf(w, "%s", "Error")
		} else {
			tmpl.Execute(w, "main.html")
		}
		//fmt.Fprintf(w, "Hello Home! %s", r.URL.Path[1:])
	})

	ProductRouter := r.PathPrefix("/chats").Subrouter()
	ProductRouter.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {

		tmpl, err := template.ParseFiles("view/allChats.html")
		if err != nil {
			fmt.Fprintf(w, "%s", "Error")
		} else {
			tmpl.Execute(w, "allChats.html")
		}
		//fmt.Fprintf(w, "All %s", "chats")

	})
	ProductRouter.HandleFunc("/{id}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		id := vars["id"]

		fmt.Fprintf(w, "You've requested the chat: %s", id)
	})

	r.HandleFunc("/post", func(w http.ResponseWriter, r *http.Request){
		fmt.Fprintf(w, "Hello %s!", r.URL.Path[1:])
	})

	r.HandleFunc("/post/{title}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		title := vars["title"]
		fmt.Fprintf(w, "You've requested the post: %s", title)
	})

	r.HandleFunc("/post/{title}/number/{number}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		title := vars["title"]
		number := vars["number"]
		fmt.Fprintf(w, "You've requested the post: %s, number: %s in a list", title, number)
	})

	r.HandleFunc("/form", func(w http.ResponseWriter, r *http.Request) {
		tmpl := template.Must(template.ParseFiles("view/form.html"))
		if r.Method != http.MethodPost {
			tmpl.Execute(w, nil)
			return
		}

		details := ContactDetails{
			Email:   r.FormValue("email"),
			Subject: r.FormValue("subject"),
			Message: r.FormValue("message"),
		}


		// do something with details
		_ = details

		tmpl.Execute(w, struct{ Success bool }{true})
	})


	http.ListenAndServe(":8080", r)
}